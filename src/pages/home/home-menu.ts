import {AlertController, ViewController} from "ionic-angular";
import {Component} from "@angular/core";

@Component({
  template: `
    <ion-list>
      <!--button ion-item (click)="theme()">Theme</button-->
      <button ion-item (click)="about()">About</button>
    </ion-list>
  `
})
export class HomeMenuPage {
  constructor(public viewCtrl: ViewController, public alertCtrl: AlertController) {}

  theme() {

  }

  about() {
    let alert = this.alertCtrl.create({
      title: 'StopWatch',
      subTitle: 'by Dr.Wolf, 2017<br>Version: 0.1a',
      buttons: ['OK']
    });
    alert.present();
    this.viewCtrl.dismiss();
  }
}
