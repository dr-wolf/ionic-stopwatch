import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'timeout'})
export class TimeoutPipe implements PipeTransform {

  private lpad(value, pad: string, length: number) {
    while (value.length < length) {
      value = pad + value;
    }
    return value;
  }

  transform(value: number): string {
    return this.lpad(Math.floor(value / 6000).toFixed(), '0', 2) + ':' + this.lpad((value % 6000 / 100).toFixed(2), '0', 5);
  }

}
