import { Component } from '@angular/core';
import {NavController, PopoverController} from 'ionic-angular';
import { Observable } from "rxjs/Rx";
import { Subscription } from "rxjs/Subscription";
import { HomeMenuPage } from "./home-menu";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  timerSubscription: Subscription;
  displayTime: number;
  time = {
    start: 0,
    stored: 0
  };

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController) {
    this.displayTime = 0;
    this.timerSubscription = null;

  }

  private timeStamp() {
    return Math.floor(new Date().getTime() / 10);
  }

  presentPopover(event) {
    let popover = this.popoverCtrl.create(HomeMenuPage);
    popover.present({
      ev: event
    });
  }

  timerControl() {
    if (this.timerSubscription) {
      this.time.stored += this.timeStamp() - this.time.start;
      this.timerSubscription.unsubscribe();
      this.timerSubscription = null;
    } else {
      this.time.start = this.timeStamp();
      this.timerSubscription = Observable.interval(10).subscribe(x => {
        this.displayTime = this.timeStamp() - this.time.start + this.time.stored;
      });
    }
  }

  timerReset() {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
      this.timerSubscription = null;
    }
    this.displayTime = 0;
    this.time.stored = 0;
  }

}
